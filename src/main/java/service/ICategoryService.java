package service;

import domain.Category;
import org.springframework.stereotype.Service;
import javax.ejb.Local;
import java.util.List;

@Local
@Service
public interface ICategoryService {

    Category createCategory(Category category);

    Category updateCategory(Category category);

    boolean deleteCategory(Category category);

    Category getCategory(int id);

    List<Category> getAllCategories();
}
