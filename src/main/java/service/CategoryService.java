package service;

import domain.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.ICategoryRepository;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
@Service
public class CategoryService implements ICategoryService {

    @Autowired
    private ICategoryRepository categoryRepository;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public Category createCategory(Category category) {
        Category createdCategory = null;
        if (isValid(category)) {
            createdCategory = categoryRepository.save(category);
        }
        return createdCategory;
    }

    @Override
    public Category updateCategory(Category category) {
        Category updatedCategory = null;
        if (isValid(category)) {
            updatedCategory = categoryRepository.save(category);
        }
        return updatedCategory;
    }

    @Override
    public boolean deleteCategory(Category category) {
        boolean isRemoved = false;
        if (isValid(category)) {
            try {
                categoryRepository.delete(category);
                isRemoved = true;
            } catch (Exception ex) {
                logger.error(ex.getMessage());
            }
        }
        return isRemoved;
    }

    @Override
    public Category getCategory(int id) {
        Category category = null;
        if (id > 0) {
            category = categoryRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        }
        return category;
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    private boolean isValid(Category category) {
        return category.getId() > 0 && !category.getName().equals("") && category.getName() != null ? true : false;
    }
}