package service;

import domain.Lottery;
import org.springframework.stereotype.Service;
import javax.ejb.Local;
import java.util.List;

@Local
@Service
public interface ILotteryService {

    Lottery createLottery(Lottery lottery);

    Lottery updateLottery(Lottery lottery);

    boolean deleteLottery(Lottery lottery);

    Lottery getLottery(int id);

    List<Lottery> getAllLotteries();
}
