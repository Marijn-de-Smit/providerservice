package rest;

import com.google.gson.Gson;
import domain.Category;
import dto.createDto.CreateCategoryDTO;
import dto.readDto.ReadCategoryDTO;
import dto.updateDto.UpdateCategoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import service.ICategoryService;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;

@Stateless
@RestController
public class CategoryController implements ICategoryController {

    @Autowired
    private ICategoryService categoryService;
    private Gson gson = new Gson();

    @PreAuthorize("hasRole('PROVIDER') or hasRole('MANAGER')")
    @Override
    public Response createCategory(String createCategoryDTO) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        CreateCategoryDTO categoryDTO = gson.fromJson(createCategoryDTO, CreateCategoryDTO.class);
        Category category = new Category(categoryDTO);

        Category createdCategory = categoryService.createCategory(category);

        if (createdCategory != null) {
            ReadCategoryDTO readCategoryDTO = new ReadCategoryDTO(createdCategory);
            response.status(Response.Status.OK).entity(readCategoryDTO);
        }
        return response.build();
    }

    @PreAuthorize("hasRole('PROVIDER') or hasRole('MANAGER')")
    @Override
    public Response updateCategory(String updateCategoryDTO) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        UpdateCategoryDTO categoryDTO = gson.fromJson(updateCategoryDTO, UpdateCategoryDTO.class);
        Category category = new Category(categoryDTO);

        Category updatedCategory = categoryService.updateCategory(category);

        if (updatedCategory != null) {
            ReadCategoryDTO readCategoryDTO = new ReadCategoryDTO(updatedCategory);
            response.status(Response.Status.OK).entity(readCategoryDTO);
        }

        return response.build();
    }

    @PreAuthorize("hasRole('PROVIDER') or hasRole('MANAGER')")
    @Override
    public Response deleteCategory(int id) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        Category category = new Category(id);

        boolean isRemoved = categoryService.deleteCategory(category);
        response.status(Response.Status.OK).entity(isRemoved);

        return response.build();
    }

    @PreAuthorize("hasRole('PROVIDER') or hasRole('MANAGER')")
    @Override
    public Response getCategory(int id) {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        Category category = categoryService.getCategory(id);

        if (category != null) {
            ReadCategoryDTO readCategoryDTO = new ReadCategoryDTO(category);
            response.status(Response.Status.OK).entity(readCategoryDTO);
        }
        return response.build();
    }

    @PreAuthorize("hasRole('PROVIDER') or hasRole('MANAGER')")
    @Override
    public Response getAllCategories() {
        Response.ResponseBuilder response = Response.status(Response.Status.BAD_REQUEST);
        List<Category> categoryList = categoryService.getAllCategories();

        if (!categoryList.isEmpty()) {
            List<ReadCategoryDTO> readCategoryDTOList = new ArrayList<>();
            for (Category categoryItem : categoryList) {
                ReadCategoryDTO readCategoryDTO = new ReadCategoryDTO(categoryItem);
                readCategoryDTOList.add(readCategoryDTO);
            }
            response.status(Response.Status.OK).entity(readCategoryDTOList);
        }
        return response.build();
    }
}
