package rest;

import org.springframework.web.bind.annotation.*;
import javax.ejb.Local;
import javax.ws.rs.core.Response;

@Local
@RequestMapping("/category")
public interface ICategoryController {

    @PostMapping("/create")
    Response createCategory(@RequestBody String createCategoryDTO);

    @PostMapping("/update")
    Response updateCategory(@RequestBody String updateCategoryDTO);

    @PostMapping("/delete")
    Response deleteCategory(@PathVariable int id);

    @GetMapping("/get/{id}")
    Response getCategory(@RequestParam int id);

    @GetMapping("/all")
    Response getAllCategories();
}
