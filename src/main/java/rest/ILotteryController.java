package rest;

import org.springframework.web.bind.annotation.*;
import javax.ejb.Local;
import javax.ws.rs.core.Response;

@Local
@RequestMapping("/lottery")
public interface ILotteryController {

    @PostMapping(value = "/create")
    Response createLottery(@RequestBody String createLotteryDTO);

    @PutMapping("/update")
    Response updateLottery(@RequestBody String updateLotteryDTO);

    @DeleteMapping("/delete/{id}")
    Response deleteLottery(@PathVariable int id);

    @GetMapping("/get/{id}")
    Response getLottery(@RequestParam int id);

    @GetMapping("/all")
    Response getAllLotteries();
}
