package dto.updateDto;

import domain.Category;

public class UpdateCategoryDTO {

    private int id;
    private String name;

    public UpdateCategoryDTO(String name, Category category) {
        this.name = name;
    }

    public int getId() { return id; }
    public String getName() {
        return name;
    }
}
