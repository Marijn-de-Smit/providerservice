package dto.updateDto;

public class UpdateLotteryDTO {

    private int id;
    private String name;
    private String category;

    public UpdateLotteryDTO(String name, String category) {
        this.name = name;
        this.category = category;
    }

    public int getId() { return id; }
    public String getName() {
        return name;
    }
    public String getCategory() {
        return category;
    }
}
