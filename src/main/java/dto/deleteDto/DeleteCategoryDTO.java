package dto.deleteDto;

public class DeleteCategoryDTO {

    private int id;

    public DeleteCategoryDTO(int id) {
        this.id = id;
    }

    public int getId() { return id; }
}
