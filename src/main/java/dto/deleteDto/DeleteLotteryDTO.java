package dto.deleteDto;

public class DeleteLotteryDTO {

    private int id;

    public DeleteLotteryDTO(int id) {
        this.id = id;
    }

    public int getId() { return id; }
}
