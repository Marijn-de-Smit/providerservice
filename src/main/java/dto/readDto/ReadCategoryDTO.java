package dto.readDto;

import domain.Category;

public class ReadCategoryDTO {

    private int id;
    private String name;

    public ReadCategoryDTO(Category category) {
        this.id = category.getId();
        this.name = category.getName();
    }

    public ReadCategoryDTO(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
