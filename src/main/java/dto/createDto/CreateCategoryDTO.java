package dto.createDto;

import domain.Category;

public class CreateCategoryDTO {

    private String name;

    public CreateCategoryDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
