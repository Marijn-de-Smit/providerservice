package authentication;

import jwt.JwtAuthentication;

public abstract class RoleAuthentication {

    public static boolean validateRole(String[] expectedRoles, JwtAuthentication authentication) {
        boolean isValidated = false;

        for (String role : expectedRoles) {
            if (role.equals(authentication.getRole())) {
                isValidated = true;
            }
        }

        return isValidated;
    }

}
