package domain;

import dto.createDto.CreateLotteryDTO;
import dto.updateDto.UpdateLotteryDTO;
import enums.Category;

import javax.persistence.*;

@Entity
public class Lottery {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int id;

    private String name;
    private Category category;

    public Lottery() {
    }

    public Lottery(int id) {
        this.id = id;
    }

    public Lottery(String name, Category category) {
        this.name = name;
        this.category = category;
    }

    public Lottery(int id, String name, Category category) {
        this.id = id;
        this.name = name;
        this.category = category;
    }

    public Lottery(CreateLotteryDTO createLotteryDTO) {
        this.name = createLotteryDTO.getName();
        this.category = Enum.valueOf(Category.class, createLotteryDTO.getCategory());
    }

    public Lottery(UpdateLotteryDTO updateLotteryDTO) {
        this.id = updateLotteryDTO.getId();
        this.name = updateLotteryDTO.getName();
        this.category = Enum.valueOf(Category.class, updateLotteryDTO.getCategory());
    }

    public int getId() {return this.id;}

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }
    public void setCategory(Category category) {
        this.category = category;
    }

}
