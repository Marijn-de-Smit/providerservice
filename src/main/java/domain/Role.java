package domain;

public enum Role {
    USER,
    PROVIDER,
    MANAGER
}
