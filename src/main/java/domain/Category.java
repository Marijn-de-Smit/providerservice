package domain;

import dto.createDto.CreateCategoryDTO;
import dto.updateDto.UpdateCategoryDTO;
import javax.persistence.*;

@Entity
public class Category {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int id;

    private String name;

    public Category() {
    }

    public Category(int id) {
        this.id = id;
    }

    public Category(String name) {
        this.name = name;
    }

    public Category(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Category(CreateCategoryDTO createCategoryDTO) {
        this.name = createCategoryDTO.getName();
    }

    public Category(UpdateCategoryDTO updateCategoryDTO) {
        this.id = updateCategoryDTO.getId();
        this.name = updateCategoryDTO.getName();
    }

    public int getId() {return this.id;}

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
