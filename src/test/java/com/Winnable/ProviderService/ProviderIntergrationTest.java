package com.Winnable.ProviderService;

import com.fasterxml.jackson.databind.ObjectMapper;
import dto.createDto.CreateLotteryDTO;
import dto.deleteDto.DeleteLotteryDTO;
import dto.readDto.ReadLotteryDTO;
import dto.updateDto.UpdateLotteryDTO;
import enums.Category;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.core.MediaType;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@WebAppConfiguration
@AutoConfigureMockMvc
@AutoConfigureTestEntityManager
public class ProviderIntergrationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private EntityManagerFactory entityManagerFactory;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void create() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .post("/lottery/create/")
                .content(asJsonString(new CreateLotteryDTO("name", "CASH")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(403));
    }

    @Test
    public void read() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .post("/lottery/all/")
                .content(asJsonString(new ReadLotteryDTO(1, "name", Category.CASH)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(403));
    }

    @Test
    public void update() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .post("/lottery/update/")
                .content(asJsonString(new UpdateLotteryDTO("name", "CASH")))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(403));
    }

    @Test
    public void delete() throws Exception {
        mvc.perform( MockMvcRequestBuilders
                .post("/lottery/delete/")
                .content(asJsonString(new DeleteLotteryDTO(1)))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().is(403));
    }


    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
