package com.Winnable.ProviderService;

import domain.Lottery;
import enums.Category;
import org.junit.Before;
import org.junit.Test;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import repository.ILotteryRepository;
import service.LotteryService;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ProviderUnitTest {

    private static final Logger logger = LoggerFactory.getLogger(ProviderUnitTest.class);

    @InjectMocks
    private LotteryService lotteryService;

    @Mock
    ILotteryRepository lotteryRepository;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void create(){
        Lottery lottery = new Lottery("test", Category.CASH);
        Lottery result = lotteryService.createLottery(lottery);
        assertEquals(result.getName(), "test");
    }

    @Test
    public void read(){
        List<Lottery> result = lotteryService.getAllLotteries();
        assertEquals(result.size(), 1);
    }

    @Test
    public void update(){
        Lottery lottery = new Lottery(1,"update", Category.CASH);
        Lottery result = lotteryService.updateLottery(lottery);
        assertEquals(result.getName(), "update");
    }

    @Test
    public void delete(){
        Lottery lottery = new Lottery(1);
        boolean result = lotteryService.deleteLottery(lottery);
        assertTrue(result);
    }
}
